#include <cstdlib>
#include <iostream>

#include <string>
using namespace std ;

class City {
private:
	class Data {
	public:
		Data(string paikkakunta = ""): iName(paikkakunta), iCount(1) {
		}
		const string &Name(void) const { 
			return iName; 
		}
		long Count() { 
			return iCount; 
		}
		long operator++() {
			++iCount;
			return iCount;
		}
		long operator--() {
			--iCount;
			return iCount;
		}
	private:
		string iName;
		long iCount;
	};

	Data* iData;

public:
	City() { 
		iData = new Data; 
	}
	City(const string &name) {
		iData = new Data(name); 
	}
	City(City &city) {
		iData++;
	}
	~City() {
		iData--;
    }
	City &operator=(City &city) {
		if (this != &city){
			if (iData-- == 0) {
				delete iData;
			}
			iData = city.iData;
			iData++;
		}
		return *this;
	}
	const string &Name() const
	{ 
		return iData->Name();
	}
};

int main() {
	City c1("TAMPERE");//creation
	City c2(c1);//copy constructor
	City c3; //default constructor
	cout << "Assignment" << endl;
	c3 = c1;
	cout << "End" << endl;
	system("PAUSE");
    return EXIT_SUCCESS;
}



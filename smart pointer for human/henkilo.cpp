#include "henkilo.h"
Henkilo::Henkilo(const string etu, const string suku, const string sotu)
: etuNimi_(etu), sukuNimi_(suku), sotu_(sotu)
{
}
Henkilo::~Henkilo(){
}
const string Henkilo::etunimi() const
{
      return etuNimi_ ;
}
const string Henkilo::sukunimi() const
{
      return sukuNimi_ ;
}
const string Henkilo::sotu() const
{
      return sotu_ ;
}
string Henkilo::infoOfSocSec(const string s) {
	if (sotu_ == s) {
		std::cout << etuNimi_ + " " + sukuNimi_ << std::endl;
		return etuNimi_ + " " + sukuNimi_;
	}
	else {
		return "";
	}
}


Henkilo_ptr::~Henkilo_ptr() {
	if (!h) delete h;
}

Henkilo_ptr::Henkilo_ptr(const string etu, const string suku, const string sotu) : etuNimi_(etu), sukuNimi_(suku), sotu_(sotu), h(0) {
}

void Henkilo_ptr::load() const {
	if (!h) h = new Henkilo(etuNimi_, sukuNimi_, sotu_);
}

Henkilo &Henkilo_ptr::operator*()const {
	load();
	return *h;
}

Henkilo *Henkilo_ptr::operator->()const {
	load();
	return h;
}

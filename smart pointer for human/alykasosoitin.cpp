#include <cstdlib>
#include <iostream>
#include "henkilo.h"
#include <vector>
#include <algorithm>
using namespace std;

int main(int argc, char *argv[]) {

	vector<Henkilo_ptr>henkiloTaulukko = vector<Henkilo_ptr>();
	henkiloTaulukko.push_back(Henkilo_ptr("Henkilo 1", "Sukunimi 1", "sotu 1"));
	henkiloTaulukko.push_back(Henkilo_ptr("Henkilo 2", "Sukunimi 2", "sotu 2"));
	henkiloTaulukko.push_back(Henkilo_ptr("Henkilo 3", "Sukunimi 3", "sotu 3"));
	henkiloTaulukko.push_back(Henkilo_ptr("Henkilo 4", "Sukunimi 4", "sotu 4"));
	henkiloTaulukko.push_back(Henkilo_ptr("Henkilo 5", "Sukunimi 5", "sotu 5"));

	vector<Henkilo_ptr>::iterator it;
	for (it = henkiloTaulukko.begin(); it != henkiloTaulukko.end(); it++) {
		(*it)->infoOfSocSec("sotu 3");
	}
    system("PAUSE");
    return EXIT_SUCCESS;
}

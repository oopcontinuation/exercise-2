#ifndef HENKILO_H
#define HENKILO_H
#include <iostream>
#include <string>
using std::string ;

class Henkilo
{
      public:
      Henkilo(const string, const string, const string );
      ~Henkilo() ;
      const string etunimi() const ;
      const string sukunimi() const ;      
      const string sotu() const ;
	  string infoOfSocSec(const string);
      private:
      string etuNimi_ ;
      string sukuNimi_ ;
      string sotu_ ;
      
};

class Henkilo_ptr {
public:
	Henkilo_ptr(const string, const string, const string);
	~Henkilo_ptr();
	Henkilo &operator*() const;
	Henkilo *operator->() const;
private:
	void load() const;
	string etuNimi_;
	string sukuNimi_;
	string sotu_;
	mutable Henkilo *h; //Dynamic resource
};
#endif

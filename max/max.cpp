#include <iostream>
#include "maximum.h"
int main(int argc, char *argv[]) {
	
	std::cout << "Max between 10 and 8: " << MAXIMUM::max(10, 8) << std::endl;
	std::cout << "Max between 5.8 and 5.9: " << MAXIMUM::max(5.8f, 5.9f) << std::endl;
	std::cout << "Max between Aakkos and Kirjat: " << MAXIMUM::max("Aakkos", "Kirjat") << std::endl;


	system("PAUSE");
	return EXIT_SUCCESS;
}
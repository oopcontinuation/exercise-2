#include "maximum.h"

namespace MAXIMUM {
	int max(const int x, const int y) {
		return x >= y ? x : y;
	};
	float max(const float x, const float y) {
		return x >= y ? x : y;
	};
	const std::string & max(const std::string & x, const std::string & y) {
		return x >= y ? x : y; // Check if x is after y in lexicographical order
	};
}
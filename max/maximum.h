#ifndef MAXIMUM_H
#define MAXIMUM_H
#include <string>
namespace MAXIMUM {
	int max(const int, const int);
	float max(const float, const float);
	const std::string & max(const std::string&, const std::string&);
}
#endif
